package com.testajava.TestaJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestaJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestaJavaApplication.class, args);
	}

}
