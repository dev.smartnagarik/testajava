package com.testajava.TestaJava.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Santosh Paudel
 */

@RestController
@RequestMapping("/test")
public class HelloWorldController {

    @GetMapping(value = "/")
    public String testing() {
        return "API is working fine";
    }
}